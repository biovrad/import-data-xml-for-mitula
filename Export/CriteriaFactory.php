<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

class CriteriaFactory
{
    public function make(array $data): CriteriaInterface
    {
        if (empty($data['consumer'])) {
            throw new \RuntimeException('Unable to create Criteria instance without consumer');
        }

        $criteriaClass = 'Criteria' . ucfirst($data['consumer']);

        if (!class_exists($criteriaClass)) {
            throw new \RuntimeException(
                sprintf(
                    'Undefined class %s',
                    $criteriaClass
                )
            );
        }

        /** @var $criteria CriteriaInterface*/
        $criteria =  new $criteriaClass();
        $criteria->setCriteria($data);

        return $criteria;
    }
}
