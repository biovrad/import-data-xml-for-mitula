<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

interface CriteriaInterface
{
    public function getConsumer(): string;

    public function setConsumer(string $consumer): void;

    public function setCriteria(array $criteria): void;
}
