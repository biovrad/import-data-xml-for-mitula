<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

class CriteriaMitula implements CriteriaInterface
{
    private $consumer;
    private $greaterThanId;

    public function getConsumer(): string
    {
        return $this->consumer;
    }

    public function setConsumer(string $consumer): void
    {
        $this->consumer = $consumer;
    }

    public function getGreaterThanId(): string
    {
        return $this->greaterThanId;
    }

    public function setGreaterThanId(string $greaterThanId): void
    {
        $this->greaterThanId = $greaterThanId;
    }

    public function setCriteria(array $criteria): void
    {
        foreach ($criteria as $criterion => $value) {
            if (method_exists($this, 'set' . $criterion)) {
                $this->{'set' . $criterion}($value);
            }
        }
    }
}
