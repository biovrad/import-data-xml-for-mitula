<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\DataProvider;

use AppBundle\Services\Export\CriteriaInterface;

interface DataProviderInterface
{
    public function getData(CriteriaInterface $criteria): array;
}
