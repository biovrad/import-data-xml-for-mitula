<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\DataProvider;

use AppBundle\Services\Export\CriteriaInterface;

class MitulaDataProvider implements DataProviderInterface
{
    private $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getData(CriteriaInterface $criteria): array
    {
        // here place data generation code
        return [];
    }
}
