<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

use AppBundle\Services\Export\DataProvider\MitulaDataProvider;
use AppBundle\Services\Export\FileGenerator\MitulaFileGenerator;
use AppBundle\Services\Export\FileSender\MitulaFileSender;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ExportServiceFactory
{
    public const MITULA = 'mitula';

    public function make(CriteriaInterface $criteria): ExportServiceInterface
    {
        $consumer = $criteria->getConsumer();
        $exportServiceClass = 'ExportService' . ucfirst($consumer);

        if (!class_exists($exportServiceClass)) {
            throw new \RuntimeException(
                sprintf(
                    'Undefined class %s',
                    $exportServiceClass
                )
            );
        }

        switch ($consumer) {
            case self::MITULA:
                return $this->makeMitulaExportService();
            default:
                throw new \RuntimeException('Could not determine ExportService factory');
        }
    }

    private function makeMitulaExportService()
    {

        $dataProvider = new MitulaDataProvider();
        $fileGenerator = new MitulaFileGenerator(new XmlEncoder());
        $fileSender = new MitulaFileSender();
        return new ExportServiceMitula($dataProvider, $fileGenerator, $fileSender);
    }
}
