<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

interface ExportServiceInterface
{
    public function getData(CriteriaInterface $criteria): array;

    public function generateResult(array $data): iterable;

    public function store(iterable $result): void;
}
