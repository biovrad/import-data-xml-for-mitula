<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

use AppBundle\Services\Export\DataProvider\DataProviderInterface;
use AppBundle\Services\Export\FileGenerator\FileGeneratorInterface;
use AppBundle\Services\Export\FileSender\FileSenderInterface;

class ExportServiceMitula implements ExportServiceInterface
{
    private $dataProvider;
    private $fileGenerator;
    private $fileSender;

    public function __construct(
        DataProviderInterface $dataProvider,
        FileGeneratorInterface $fileGenerator,
        FileSenderInterface $fileSender
    )
    {
        $this->dataProvider = $dataProvider;
        $this->fileGenerator = $fileGenerator;
        $this->fileSender = $fileSender;
    }

    public function getData(CriteriaInterface $criteria): array
    {
        return $this->dataProvider->getData($criteria);
    }

    public function generateResult(array $data): iterable
    {
        return $this->fileGenerator->generate($data);
    }

    public function store(iterable $result): void
    {
        $this->fileSender->send($result);
    }
}
