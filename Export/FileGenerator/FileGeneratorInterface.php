<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\FileGenerator;


interface FileGeneratorInterface
{
    public function generate(array $data): iterable;
}
