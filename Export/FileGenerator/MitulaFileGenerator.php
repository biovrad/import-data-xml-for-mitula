<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\FileGenerator;


use Symfony\Component\Serializer\Encoder\XmlEncoder;

class MitulaFileGenerator implements FileGeneratorInterface
{
    private $encoder;

    public function __construct(XmlEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    public function generate(array $data): iterable
    {
        // here the code for xml file generation

        return tmpfile();
    }
}
