<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\FileSender;

interface FileSenderInterface
{
    public function send(iterable $filename): void;
}
