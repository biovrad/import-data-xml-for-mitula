<?php

declare(strict_types=1);

namespace AppBundle\Services\Export\FileSender;

class MitulaFileSender implements FileSenderInterface
{
    public function send(iterable $filename): void
    {
        //here is the code that sends file to AWS
    }
}
