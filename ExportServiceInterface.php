<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;


interface ExportServiceInterface
{
    public function generateFiles(array $criteria): void;
}
