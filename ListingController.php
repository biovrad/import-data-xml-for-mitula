<?php

declare(strict_types=1);

namespace AppBundle\Controller\Exposed;

use AppBundle\Controller\AbstractController;
use AppBundle\Exceptions\ListingException;
use AppBundle\Factory\Export\CriteriaFactory;
use AppBundle\Factory\Export\MitulaExportServiceFactory;
use AppBundle\Model\Listing\RecentForHomePage\Listing as RecentListing;
use AppBundle\Model\Listing\Search\Search;
use AppBundle\Model\Listing\Search\SearchSimilar;
use AppBundle\Model\Request\Summary\Summary;
use AppBundle\Services\ListingServiceInterface;
use AppBundle\Util\Cache\Counter\CounterServiceInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/public/listings")
 */
class ListingController extends AbstractController
{
    private $service;
    private $listingsCounter;
    private $cr;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        ListingServiceInterface $service,
        CounterServiceInterface $listingsCounter,
        MitulaExportServiceFactory $s
    ) {
        parent::__construct($serializer, $validator);
        $this->s = $s;
        $this->service = $service;
        $this->listingsCounter = $listingsCounter;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function search(Request $request): Response
    {
        $criteria = [
            'market' => 'za',
            'type' => 'residential',
            'order' => 'ASC',
            'id' => '0000acf5-61a4-4139-8c86-ef93e3e73d5e',
            'limit' => 2,
            'consumer' => 'mitula',
        ];

        $criteriaFactory = new CriteriaFactory();
        $mitulaCriteria = $criteriaFactory->make($criteria);
        $exportServiceFactory = $this->s;
        $exportService = $exportServiceFactory->make($mitulaCriteria);
        $data = $exportService->getData($mitulaCriteria);
        $file = $exportService->generateResult($data);
        $exportService->store($file);

        $this->cr->generateFiles($criteria);
        /** @var Search $model */
        $model = $this->validatePayload(json_encode($request->query->all()), Search::class);
        if ($model instanceof Response) {
            return $model;
        }

        $page = $request->query->getInt('page') ?: null;

        return $this->createSuccessResponse($this->service->search($model, $page));
    }

    /**
     * @Route("/recent/", methods={"GET"})
     */
    public function recentCompanyListings(Request $request): Response
    {
        /** @var RecentListing $model */
        $model = $this->validatePayload(json_encode($request->query->all()), RecentListing::class);
        if ($model instanceof Response) {
            return $model;
        }

        try {
            $listings = $this->service->getListingsForHomePage($model);
        } catch (ListingException $e) {
            return $this->createFailureResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->createSuccessResponse($listings);
    }

    /**
     * @Route("/similar/", methods={"GET"})
     */
    public function similar(Request $request): Response
    {
        $model = $this->validatePayload(json_encode($request->query->all()), SearchSimilar::class);
        if ($model instanceof Response) {
            return $model;
        }

        return $this->createSuccessResponse($this->service->searchSimilar($model));
    }

    /**
     * @Route("/shortcode/{shortCode}/", methods={"GET"})
     */
    public function getListingByShortCode(string $shortCode): Response
    {
        try {
            $listing = $this->service->findOneByShortCode($shortCode);
        } catch (ListingException $e) {
            return $this->createFailureResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->createSuccessResponse($listing);
    }

    /**
     * @Route("/summary/", methods={"GET"})
     */
    public function summaryAction(Summary $summaryModel): Response
    {
        try {
            $result = $this->listingsCounter->getCountByCountryCodeAndListingType($summaryModel);
        } catch (ListingException $exception) {
            return $this->createFailureResponse([$exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return $this->createSuccessResponse($result);
    }

    /**
     * @Route("/slug/{slug}/", methods={"GET"})
     */
    public function getListingBySlug(string $slug): Response
    {
        try {
            $result = $this->service->findOneBySlug($slug);
        } catch (ListingException $e) {
            return $this->createFailureResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->createSuccessResponse($result);
    }

    /**
     * @Route("/{id}/", methods={"GET"})
     */
    public function show(string $id): Response
    {
        try {
            $result = $this->service->find($id);
        } catch (ListingException $e) {
            return $this->createFailureResponse([$e->getMessage()], Response::HTTP_NOT_FOUND);
        }

        return $this->createSuccessResponse($result);
    }
}
