<?php

declare(strict_types=1);

namespace AppBundle\Services\Export;

use AppBundle\Repository\ListingRepositoryInterface;
use AppBundle\Util\Aws\AwsS3Util;
use AppBundle\Util\ServiceLocatorUtilInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use ZipArchive;


class MitulaExportService implements ExportServiceInterface
{
    private $serviceLocator;
    private $url;

    public function __construct(
        ServiceLocatorUtilInterface $serviceLocator, $url) {
        $this->serviceLocator = $serviceLocator;
        $this->url = $url;
    }

    public function generateFiles(array $criteria): void
    {
        $fileName = 'commercialpeople_mitula_'.date('Y_m_d_B');

        $xmlString = $this->getXMLContent($criteria);
        $tmpFiles = $this->createTmpFile($xmlString);
        $this->uploadFilesToS3($tmpFiles, $fileName);
    }

    private function addFilesToArchive(string $tmpFiles, string $fileName): string
    {
        $zip = new ZipArchive();

        $fileNameArchive = $fileName.'.zip';
        $zip->open($fileNameArchive, ZipArchive::CREATE);
        $zip->addFile($tmpFiles, $fileName.'.xml');
        $zip->close();

        return $fileNameArchive;
    }

    private function uploadFilesToS3(string $tmpFiles, string $fileName): void {

        $fileNameArchive = $this->addFilesToArchive($tmpFiles, $fileName);

        /** @var AwsS3Util $awsS3Util */
        $awsS3Util = $this->serviceLocator->locate(AwsS3Util::class);
        $awsS3Util->putObject(
            'listing-mitula',
            $fileNameArchive,
            $fileName.'.zip',
            true
        );
        unlink($fileName.'.zip');
    }

    private function getXMLContent(array $criteria): string
    {
        /** @var ListingRepositoryInterface $listingRepository */
        $listingRepository = $this->serviceLocator->locate(ListingRepositoryInterface::class);

        $result = $listingRepository->findAllByCriteriaListings($criteria);

        if (empty($result)) {
            return '';
        }

        $xmlResAd = [];
        foreach ($result as $val){

            /** @var \DateTime $createData */
            $createData = $val[0]['createdAt'];
            $date = $createData->format('d/m/Y');
            $time = $createData->format('H:i');

            $xmlResAd[] = [
                    'id' => "<{$val[0]['id']}>",
                    'url' => $this->getListingUrl($val[0]['countryCode'], $val[0]['slug']),
                    'title' => "<{$val[0]['displayAddress']}>",
                    'type' => "<for {$val[0]['transactionType']}>",
                    'agency' => "<{$val['name']}>",
                    'content' => "<{$val[0]['description']}>",
                    'price' => "<{$val[0]['price.minimum']}>",
                    'floor_area' => "<{$val[0]['size.minimum']}, {$val[0]['size.maximum']}>",
                    'rooms' => "<{$val[0]['receptionRooms']}, {$val[0]['bedrooms']}>",
                    'bathrooms' => "<{$val[0]['bedrooms']}>",
                    'address' => "<{$val[0]['address.address1']}, {$val[0]['address.address2']}>",
                    'city' => "<{$val[0]['address.town']}>",
                    'postcode' => "<{$val[0]['address.postcode']}>",
                    'country' => "<{$val[0]['address.country']}>",
                    'latitude' => "<{$val[0]['coordinates.latitude']}>",
                    'longitude' => "<{$val[0]['coordinates.longitude']}>",
                    'pictures' => [
                        'picture' => $this->createPictureUrlElements($val[0]['images']),
                    ],
                    'date' => "<{$date}>",
                    'time' => "<{$time}>",
            ];
        }

        $xmlData = [
            'Mitula' => [
                'ad' => $xmlResAd
            ]
        ];

        $xmlEncoder = new XmlEncoder();
        $xml = $xmlEncoder->encode($xmlData, 'xml');
        $response = new Response();
        $response->setContent($xml);
        $response->headers->set('Content-Type', 'xml');
        $response->headers->set('encoding', 'utf-8');

        return $response->getContent();
    }

    private function getListingUrl(string $countryCode, string $slug): string
    {
        return "<https://www.{$this->url}/{$countryCode}/property/{$slug}>";
    }

    private function createTmpFile(string $xmlString): string
    {
        $tmpFile = tempnam('/tmp', '_mitula_');
        file_put_contents($tmpFile, $xmlString, FILE_APPEND);
        return $tmpFile;
    }

    protected function createPictureUrlElements(array $val): array
    {
     $pic = [];
        foreach ($val as $valImg) {
            $pic[] = "<https://www.{$this->url}{$valImg}>";
        }

        return ['picture_url' => $pic];
    }
}
